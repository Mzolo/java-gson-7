import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.time.LocalDate;

public class GsonDemo {
@JsonAdapter(EmployeeAdapter.class)
/*
 * The JsonAdapter annotation type is used with a TypeAdapter
 * Class object argument to associate the TypeAdapter instance
 * to use with a class or field. Doing so you don't need to
 * register the TypeAdapter with Gson - more simple and less coding
 */
        static class Employee {
            String name;
            LocalDate hireDate;
            // constructors
            Employee() {}
            Employee(String name, LocalDate hireDate) {
                this.name = name;
                this.hireDate = hireDate;
            }
        }
        // using a TypeAdapter and API streaming
        static class EmployeeAdapter extends TypeAdapter<Employee> {
            @Override
            // Deserialize (write): Parsing Json object ---> Java object
            public void write(JsonWriter jsonWriter, Employee employee) throws IOException {
                System.out.println("EmployeeAdapter write() method called");
                jsonWriter.beginObject();
                jsonWriter.name("name").value(employee.name);
                jsonWriter.name("hireDate").value(employee.hireDate.toString());
                jsonWriter.endObject();
            }

            @Override
            // Serialize (read) : Parsing Java Object ---> Json Object
            public Employee read(JsonReader jsonReader) throws IOException {
                System.out.println("EmployeeAdapter read() method called ");
                Employee employee = new Employee();
                jsonReader.beginObject();
                while (jsonReader.hasNext())
                    switch (jsonReader.nextName()) {
                        case "name":
                            employee.name = jsonReader.nextString();
                            break;
                        case "hireDate":
                            employee.hireDate = LocalDate.parse(jsonReader.nextString());
                }
                jsonReader.endObject();
                return employee;
                }
            }

        public static void main(String[] args) {
            Gson gson = new Gson();
            Employee employee = new Employee("Tony Allen",
                    LocalDate.of(2017, 9,21));
            // Serialize the Java object(employee) to Json
            String json = gson.toJson(employee);
            System.out.println(json);
            employee = gson.fromJson(json, employee.getClass());
            System.out.printf("Name = %s%n", employee.name );
            System.out.printf("hireDate = %s%n", employee.hireDate);
            System.out.println();
        } // end of main(String[] args) method

} // end of GsonDemo class
